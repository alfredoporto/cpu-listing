# CPU-Listing

Step 1: Compile the spring boot application into a .jar -- Use mvn clean install -DskipTests=true

Step 2: Compile the angular app -- Use ng build at the root of the application

Step 3: Run Docker compose: docker-compose -f docker-compose.yml (up / -d)

Step 4: Insert some dummy data

Step 4.1: Connect to mysql docker image

    docker exec -it <mysql-docker-id> bash

Step 4.2: Connect to mysql db

    # mysql -u test -p 

Step 4.3: Insert some dummy data like
    

    insert into socket (id, name) values (1,'AM4');
    insert into socket (id, name) values (2,'DIP');
    insert into cpu (id,brand,model,img,socket_id,clockspeed,number_of_cores, number_of_threads,tdp,price,date_creation,date_modification) values (1,'intel','ice-lake','image',1,3.4,4,8,56,209.5,NOW(),NOW());
    commit;
    exit;

Final Step: Go to localhost:4200


(*)Tests are missing due to some predicaments, will be added later on. 
