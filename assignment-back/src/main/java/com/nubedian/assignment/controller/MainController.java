package com.nubedian.assignment.controller;

import com.nubedian.assignment.entity.Cpu;
import com.nubedian.assignment.entity.Socket;
import com.nubedian.assignment.service.cpu.CpuService;
import com.nubedian.assignment.service.socket.SocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin

public class MainController {

    @Autowired
    private CpuService cpuService;
    @Autowired
    private SocketService socketService;


    @GetMapping("/test")
    public String test(){
        return "working...";
    }

    @GetMapping("/cpus")
    public List<Cpu> listCpu(){
        //System.out.println("GAAA");
        return cpuService.getAllCpus();
    }

    @GetMapping("/cpus/{cpuId}")
    public Cpu getCpu(@PathVariable int cpuId){
        Cpu cpu = cpuService.getCpuById(cpuId);

        if(cpu == null){
            throw new RuntimeException("Cpu not found" + cpuId);
        }
        return cpu;
    }


    @PutMapping("/cpus/{id}")
    public Cpu updateCpu(@PathVariable("id")int id, @RequestBody Cpu cpu) {

        cpuService.updateCpu(cpu);

        return cpuService.getCpuById(id);
    }
    @GetMapping("/sockets")
    public List<Socket> getSockets(){
        return this.socketService.getAllSockets();
    }
}
