package com.nubedian.assignment.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name="Cpu")
public class Cpu implements Serializable{
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    private int id;
    private String brand;
    private String model;
    private float clockspeed;
    private int numberOfCores;
    private int numberOfThreads;
    private int tdp;
    private float price;
    private Date dateCreation;
    private Date dateModification;
    private String img;

    @ManyToOne
    private Socket socket;



    public Cpu(){}

    public Cpu(int id, String brand, String model, Socket sockets, float clockspeed, int numberOfCores, int numberOfThreads, int tdp, float price, Date dateCreation, Date dateModification, String img) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.socket = sockets;
        this.clockspeed = clockspeed;
        this.numberOfCores = numberOfCores;
        this.numberOfThreads = numberOfThreads;
        this.tdp = tdp;
        this.price = price;
        this.dateCreation = dateCreation;
        this.dateModification = dateModification;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket sockets) {
        this.socket = sockets;
    }




    public float getClockspeed() {
        return clockspeed;
    }

    public void setClockspeed(float clockspeed) {
        this.clockspeed = clockspeed;
    }

    public int getNumberOfCores() {
        return numberOfCores;
    }

    public void setNumberOfCores(int numberOfCores) {
        this.numberOfCores = numberOfCores;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public int getTdp() {
        return tdp;
    }

    public void setTdp(int tdp) {
        this.tdp = tdp;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
//brand, model, socket, clockspeed, numberOfCores
    //numberOfThreads, tdp, price

}
