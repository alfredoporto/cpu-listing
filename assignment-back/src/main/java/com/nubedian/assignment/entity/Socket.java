package com.nubedian.assignment.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;



@Entity(name="socket")
public class Socket implements Serializable {
    @Id
    private int id;
    private String name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Socket(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Socket(){}
}
