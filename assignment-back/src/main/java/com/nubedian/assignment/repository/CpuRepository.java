package com.nubedian.assignment.repository;

import com.nubedian.assignment.entity.Cpu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("cpuRepository")
public interface CpuRepository extends JpaRepository<Cpu, Integer> {
}
