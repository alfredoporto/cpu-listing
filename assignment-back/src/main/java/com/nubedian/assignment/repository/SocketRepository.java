package com.nubedian.assignment.repository;

import com.nubedian.assignment.entity.Socket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("socketRepository")
public interface SocketRepository extends JpaRepository<Socket, Integer> {
}
