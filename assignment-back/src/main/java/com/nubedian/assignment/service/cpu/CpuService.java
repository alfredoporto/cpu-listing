package com.nubedian.assignment.service.cpu;

import com.nubedian.assignment.entity.Cpu;


import java.util.List;

public interface CpuService {
    public List<Cpu> getAllCpus();

    public Cpu getCpuById(int id);

    public boolean updateCpu(Cpu cpu);


}
