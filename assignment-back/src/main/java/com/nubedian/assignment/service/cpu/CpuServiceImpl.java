package com.nubedian.assignment.service.cpu;

import com.nubedian.assignment.entity.Cpu;
import com.nubedian.assignment.repository.CpuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CpuServiceImpl implements CpuService{

    @Autowired
    private CpuRepository cpuRepository;

    @Override
    public List<Cpu> getAllCpus() {
        List<Cpu> listCpus = cpuRepository.findAll();
        return listCpus;
    }

    @Override
    public Cpu getCpuById(int id) {
        Cpu cpu = cpuRepository.findById(id).get();
        return cpu;
        //return null;
    }

    @Override
    public boolean updateCpu(Cpu cpu) {
        Cpu cpu1 = cpuRepository.findById(cpu.getId()).get();
        if(cpu1 != null){
            return cpuRepository.save(cpu) == cpu;
        }
        return false;

    }

}
