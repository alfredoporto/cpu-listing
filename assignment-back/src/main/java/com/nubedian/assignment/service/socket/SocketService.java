package com.nubedian.assignment.service.socket;

import com.nubedian.assignment.entity.Socket;

import java.util.List;

public interface SocketService {
    public List<Socket> getAllSockets();
}
