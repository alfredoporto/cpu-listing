package com.nubedian.assignment.service.socket;

import com.nubedian.assignment.entity.Socket;
import com.nubedian.assignment.repository.SocketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SocketServiceImpl implements SocketService{
    @Autowired
    private SocketRepository socketRepository;

    @Override
    public List<Socket> getAllSockets() {
        List<Socket> sockets = socketRepository.findAll();
        return sockets;
    }
}
