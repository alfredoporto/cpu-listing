import {RouterModule, Routes} from "@angular/router";
import {CpuComponent} from "../cpu/cpu.component";
import {CpuDetailComponent} from "../cpu-detail/cpu-detail.component";



export const routes: Routes = [
  {path: 'cpu', component: CpuComponent},
  {path: 'cpu/:id', component: CpuDetailComponent},
  {path: '', redirectTo: 'cpu', pathMatch: 'full'},
  {path: '**', redirectTo: 'cpu', pathMatch: 'full'}

];

