import { Component, OnInit } from '@angular/core';
import {CpuService} from "../services/cpu.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-cpu-detail',
  templateUrl: './cpu-detail.component.html',
  styleUrls: ['./cpu-detail.component.css']
})
export class CpuDetailComponent implements OnInit {

  cpu: any;
  sockets: any;
  id=0;
  message ="";
  constructor(private cs:CpuService,
              private route:ActivatedRoute,
              private router:Router,
              private location:Location) { }



  ngOnInit(){
    this.id = this.route.snapshot.params.id;
    if(this.id){
      this.getCpu();
      this.getSockets();
    }

  }

  getCpu(){
    this.cs.getCpu(this.id)
      .subscribe(response => this.cpu=response, error => this.message=error);
  }

  saveCpu(){
    this.cs.putCPu(this.cpu)
      .subscribe(response=>{
        this.router.navigate(['/cpu']);
        },error => this.message=error);
  }
  goBack():void{
    this.location.back();
  }
  getSockets(){
    this.cs.getSockets()
      .subscribe(response => this.sockets=response, error=>this.message=error)
  }

}
