import {Component, Inject, OnInit} from '@angular/core';
import {CpuService} from "../services/cpu.service";

@Component({
  selector: 'app-cpu',
  templateUrl: './cpu.component.html',
  styleUrls: ['./cpu.component.css']
})
export class CpuComponent implements OnInit {
  title = "List of CPUs"
  cpus: any;
  message = "";


  constructor(private cs:CpuService) { }

  ngOnInit(): void {
    this.getCpus();
  }

  getCpus(){
    this.cs.getCpus()
      .subscribe(response => this.cpus=response, error => this.message = error);
  }

}
