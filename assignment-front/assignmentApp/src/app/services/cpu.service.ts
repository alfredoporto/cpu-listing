import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ProcessHttpmsgService} from "./process-httpmsg.service";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";

import {baseURL} from "../shared/baseurl";

@Injectable({
  providedIn: 'root'
})
export class CpuService {

  constructor(private http:HttpClient, private processHttpMsgService: ProcessHttpmsgService) { }

  getCpus(): Observable<any>{
    return this.http.get(baseURL + 'cpus')
      .pipe(catchError(this.processHttpMsgService.handleError));
  }

  getCpu(id: number): Observable<any>{
    return this.http.get(baseURL + 'cpus/' +id)
      .pipe(catchError(this.processHttpMsgService.handleError));
  }

  putCPu(cpu: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.put(baseURL + 'cpus/' + cpu.id, cpu, httpOptions)
      .pipe(catchError(this.processHttpMsgService.handleError));;

  }
  getSockets(): Observable<any>{
    return this.http.get(baseURL + 'sockets')
      .pipe(catchError(this.processHttpMsgService.handleError));
  }
}
